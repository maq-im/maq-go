package main

import (
	"gitlab.com/maq-im/maq-go"
	"gitlab.com/maq-im/maq-go/interop"
	"log"
)

func main() {
	client := maq.NewClient()

	client.Authorize()

	for {
		status, _ := client.AuthorizationStatus()

		if status == interop.AuthStatus_WAIT_USERNAME {
			err := client.SendUserIdentifier("test")
			if err != nil {
				log.Println("Error sending userid:", err)
			}
		} else if status == interop.AuthStatus_WAIT_PASSWORD {
			err := client.SendPassword("testpassword")
			if err != nil {
				log.Println("Error sending password:", err)
			}
		} else if status == interop.AuthStatus_READY {
			log.Println("CLIENT READY")
			break
		}
	}
}
