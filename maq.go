package maq

// #cgo CFLAGS: -I/home/bhat/.local/go/src/gitlab.com/maq-im/maq-go/
// #cgo LDFLAGS: /home/bhat/.local/go/src/gitlab.com/maq-im/maq-go/libmaq.a -lstdc++ -lssl -lcrypto -ldl -lm
// #include <maq.h>
import "C"
import (
	"errors"
	"github.com/golang/protobuf/proto"
	"gitlab.com/maq-im/maq-go/interop"
	"log"
	"math/rand"
	"sync"
	"time"
	"unsafe"
)

type Client struct {
	c       *C.Client_t
	waiters sync.Map
}

func NewClient() *Client {
	c := C.new_client(C.CString("https://matrix.encom.eu.org"), C.CString("/tmp/maq/db"), C.CString("/tmp/maq/cache"))

	client := Client{
		c: c,
	}

	go func() {
		for {
			recvInfo, err := client.Receive()
			if err != nil {
				log.Println("Error received:", err)
				continue
			}

			if rep := recvInfo.GetRep(); rep != nil {
				if waiter, found := client.waiters.Load(rep.Extra); found {
					// found? send it to waiter channel
					waiter.(chan *interop.Rep) <- rep

					close(waiter.(chan *interop.Rep))
				}
			}
		}
	}()

	return &client
}

func (c *Client) Receive() (*interop.ReceivedInfo, error) {
	resultBytesC := C.client_recv_rep(c.c, C.uint(10))
	resultBytes := C.GoBytes((unsafe.Pointer)(resultBytesC.ptr), (C.int)(resultBytesC.len))
	C.bytes_destroy(resultBytesC)

	recvInfo := &interop.ReceivedInfo{}
	if err := proto.Unmarshal(resultBytes, recvInfo); err != nil {
		return nil, err
	}

	return recvInfo, nil
}

func (c *Client) SendRequest(request *interop.Req) (*interop.Rep, error) {
	letters := "qwertyuiopasdfghjklzxcvbnm"
	extraBytes := make([]byte, 32)
	for i := range extraBytes {
		extraBytes[i] = letters[rand.Intn(len(letters))]
	}
	extra := string(extraBytes)
	request.Extra = extra

	reqBytes, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	reqBytesC := C.slice_ref_uint8_t{(*C.uchar)(unsafe.Pointer(&reqBytes[0])), C.ulong(len(reqBytes))}

	waiter := make(chan *interop.Rep, 1)
	c.waiters.Store(extra, waiter)

	C.client_send_req(c.c, reqBytesC)

	select {
	case response := <-waiter:
		return response, nil
	case <-time.After(10 * time.Second):
		c.waiters.Delete(extra)
		return nil, errors.New("recv timeout")
	}
}

func (c *Client) Authorize() error {
	request := &interop.Req{
		Entry: &interop.Req_Authorize{
			Authorize: &interop.Authorize_Req{},
		},
	}

	_, err := c.SendRequest(request)

	return err
}

func (c *Client) AuthorizationStatus() (interop.AuthStatus, error) {
	request := &interop.Req{
		Entry: &interop.Req_AuthorizationStatus{
			AuthorizationStatus: &interop.AuthorizationStatus_Req{},
		},
	}

	result, err := c.SendRequest(request)
	if err != nil {
		return interop.AuthStatus_UNKNOWN, err
	}

	return result.GetAuthorizationStatus().Status, nil
}

func (c *Client) SendUserIdentifier(userIdentifier string) error {
	request := &interop.Req{
		Entry: &interop.Req_SendUserIdentifier{
			SendUserIdentifier: &interop.SendUserIdentifier_Req{
				UserIdentifier: userIdentifier,
			},
		},
	}

	_, err := c.SendRequest(request)

	return err
}

func (c *Client) SendPassword(password string) error {
	request := &interop.Req{
		Entry: &interop.Req_SendPassword{
			SendPassword: &interop.SendPassword_Req{
				Password: password,
			},
		},
	}

	_, err := c.SendRequest(request)

	return err
}
